from flask import Flask,render_template
from flask import request, redirect 
from flask_sqlalchemy import SQLAlchemy

  
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
db = SQLAlchemy(app)

class Blog(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    author = db.Column(db.Text)
    content = db.Column(db.Text)
    title = db.Column(db.Text)

    def __init__(self, title, content, author):
        self.content = content
        self.title = title 
        self.author = author

db.create_all()


@app.route('/')
def list_blog():
        blogs=Blog.query.all()
        return render_template('list.html',blogs=blogs)
        
@app.route("/add",methods=["POST"])
def add_blog():
    title = request.form["title"]
    content = request.form["content"]
    author =  request.form["author"]
    if title and content and author:
        blog = Blog(title, content, author)
        db.session.add(blog)
        db.session.commit()
        return redirect("/")
    else:
        return redirect("/add")

if __name__=="__main__":
   app.run()
